<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link href="https://fonts.googleapis.com/css?family=Miriam+Libre" rel="stylesheet">
    <link rel="stylesheet" href="css/fonts.css" >
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="css/custom.css" >

    <!-- Custom styles for this template-->
    <title>Hello, world!</title>
</head>
<body>
<?php include('header.html'); ?>
<div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner">
        <div class="carousel-item active">
            <img class="d-block w-100" src="images/shanghai.jpg" alt="First slide">
            <div class="carousel-caption d-none d-md-block">
                <div class="container">
                    <div class="row">
                        <div class="col"></div>
                        <div class="col-6 caption-title">
                            <h2>Компактні трактори Dongfeng</h2>
                            <a href="#">Відвідати сторінку ></a>
                        </div>
                        <div class="col-1"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="carousel-item">
            <img class="d-block w-100" src="images/shanghai.jpg" alt="Second slide">
            <div class="carousel-caption d-none d-md-block">
                <div class="container">
                    <div class="row">
                        <div class="col"></div>
                        <div class="col-6 caption-title">
                            <h2>Компактні трактори Dongfeng</h2>
                            <a href="#">Відвідати сторінку ></a>
                        </div>
                        <div class="col-1"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="carousel-item">
            <img class="d-block w-100" src="images/shanghai.jpg" alt="Third slide">
            <div class="carousel-caption d-none d-md-block">
                <div class="container">
                    <div class="row">
                        <div class="col"></div>
                        <div class="col-6 caption-title">
                            <h2>Компактні трактори Dongfeng</h2>
                            <a href="#">Відвідати сторінку ></a>
                        </div>
                        <div class="col-1"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>
<div class="container" id="main">
    <div class="inner-nav-wrapper">
        <div class="d-flex justify-content-center">
            <div class="inner-nav">
                <div class="row">
                    <div class="col-12">
                        <p> Плануєш придбати продукцію агро-захід?</p>
                    </div>
                </div>
                <div style="padding: 0 21px;">
                    <div class="row">
                        <div class="col-lg-3 nav-icon-block">
                            <div>
                                <i class="nav-icon price"></i>
                            </div>
                            <a href="#" role="button">Heading</a>
                        </div>
                        <div class="col-lg-3 nav-icon-block">
                            <div>
                                <i class="nav-icon compare"></i>
                            </div>
                            <a href="#" role="button">Heading</a>
                        </div><!-- /.col-lg-4 -->
                        <div class="col-lg-3 nav-icon-block">
                            <div>
                                <i class="nav-icon find"></i>
                            </div>
                            <a href="#" role="button">Heading</a>
                        </div>
                        <div class="col-lg-3 nav-icon-block">
                            <div class="">
                                <i class="nav-icon docs"></i>
                            </div>
                            <a href="#" role="button">Heading</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="products-container">
        <div class="row">
                <div class="col-4">
                    <a href="#" class="h4"> Пропозиції </a>
                </div>
                <div class="col-4">
                    <a href="#" class="h4"> Що нового </a>
                </div>
                <div class="col-4">
                    <a href="#" class="h4"> Ми в соцмережах </a>
                </div>
        </div>
        <div class="row">
            <div class="col-4 product-item">
                <img class="" src="images/car-2.png" alt="">
                <p> Напівпричіп тракторний 1ПТС-2 </p>
                <p> Будова: рама зварна конструкція, яка складається з двох боковин , зєднаних між собою двома поперечинами, стояків, балки гідропідйомників, кронштейнів , кутників, тримачів, підсилювачів, запобіжного ланцюга. </p>
            </div>
            <div class="col-4 product-item">
                <img class="" src="images/car-2.png" alt="">
                <p> Напівпричіп тракторний 1ПТС-2 </p>
                <p> Будова: рама зварна конструкція, яка складається з двох боковин , зєднаних між собою двома поперечинами, стояків, балки гідропідйомників, кронштейнів , кутників, тримачів, підсилювачів, запобіжного ланцюга. </p>
            </div>
            <div class="col-4 product-item">
                <img class="" src="images/car-2.png" alt="">
                <p> Напівпричіп тракторний 1ПТС-2 </p>
                <p> Будова: рама зварна конструкція, яка складається з двох боковин , зєднаних між собою двома поперечинами, стояків, балки гідропідйомників, кронштейнів , кутників, тримачів, підсилювачів, запобіжного ланцюга. </p>
            </div>
            <div class="col-4 product-item">
                <img class="" src="images/car-2.png" alt="">
                <p> Напівпричіп тракторний 1ПТС-2 </p>
                <p> Будова: рама зварна конструкція, яка складається з двох боковин , зєднаних між собою двома поперечинами, стояків, балки гідропідйомників, кронштейнів , кутників, тримачів, підсилювачів, запобіжного ланцюга. </p>
            </div>
            <div class="col-4 product-item">
                <img class="" src="images/car-2.png" alt="">
                <p> Напівпричіп тракторний 1ПТС-2 </p>
                <p> Будова: рама зварна конструкція, яка складається з двох боковин , зєднаних між собою двома поперечинами, стояків, балки гідропідйомників, кронштейнів , кутників, тримачів, підсилювачів, запобіжного ланцюга. </p>
            </div>
            <div class="col-4 product-item">
                <img class="" src="images/car-2.png" alt="">
                <p> Напівпричіп тракторний 1ПТС-2 </p>
                <p> Будова: рама зварна конструкція, яка складається з двох боковин , зєднаних між собою двома поперечинами, стояків, балки гідропідйомників, кронштейнів , кутників, тримачів, підсилювачів, запобіжного ланцюга. </p>
            </div>
        </div>
    </div>
</div>

<?php include('footer.html') ?>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
<script>
    $('.carousel').carousel({
        interval: 10000000
    })
</script>
</body>
</html>