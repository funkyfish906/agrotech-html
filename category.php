<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link href="https://fonts.googleapis.com/css?family=Miriam+Libre" rel="stylesheet">
    <link rel="stylesheet" href="css/fonts.css" >
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="css/custom.css" >

    <!-- Custom styles for this template-->
    <title>Hello, world!</title>
</head>
<body class="blue-bg">
<?php include('header.html'); ?>
<div class="container" id="main">
    <div class="filters-wrapper">
        <div class="filters">
            <form class="form">
                <div class="row">
                    <div class="col-5">
                        <div class="form-group">
                            <label> Бренди: </label>
                            <select>
                                <option> brand1 </option>
                                <option> brand2 </option>
                                <option> brand3 </option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label> Кількість циліндрів: </label>
                            <select>
                                <option> brand1 </option>
                                <option> brand2 </option>
                                <option> brand3 </option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label> Кабіна: </label>
                            <select>
                                <option> brand1 </option>
                                <option> brand2 </option>
                                <option> brand3 </option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label> Потужність: </label>
                            <select>
                                <option> brand1 </option>
                                <option> brand2 </option>
                                <option> brand3 </option>
                            </select>
                        </div>
                    </div>
                    <div class="col-7">
                        <div class="form-group">
                            <label> Зчеплення: </label>
                            <select>
                                <option> brand1 </option>
                                <option> brand2 </option>
                                <option> brand3 </option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label> Ціна: </label>
                             від <input type="text">
                             до <input type="text">
                        </div>
                        <div class="form-group">
                            <label> Колісна формула: </label>
                            <select>
                                <option> brand1 </option>
                                <option> brand2 </option>
                                <option> brand3 </option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label> Блокування заднього диферинціалу: </label>
                            <select>
                                <option> brand1 </option>
                                <option> brand2 </option>
                                <option> brand3 </option>
                            </select>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="products-container">
        <nav aria-label="Page navigation example">
            <ul class="pagination">
                <li class="page-item">
                    <a class="page-link" href="#"><<</a>
                </li>
                <li class="page-item">
                    <a class="page-link" href="#">1</a>
                </li>
                <li class="page-item">
                    <a class="page-link" href="#">2</a>
                </li>
                <li class="page-item">
                    <a class="page-link" href="#">3</a>
                </li>
                <li class="page-item">
                    <a class="page-link" href="#">>></a>
                </li>
            </ul>
        </nav>
        <div class="row">
            <div class="col-4 product-item">
                <h4> Dongfeng DF-244LUX </h4>
                <img class="" src="images/car-2.png" alt="">
                <p> Напівпричіп тракторний 1ПТС-2 </p>
                <p> Будова: рама зварна конструкція, яка складається з двох боковин , зєднаних між собою двома поперечинами, стояків, балки гідропідйомників, кронштейнів , кутників, тримачів, підсилювачів, запобіжного ланцюга. </p>
                <a class="btn more">Дательніше</a>
            </div>
            <div class="col-4 product-item">
                <h4> Dongfeng DF-244LUX </h4>
                <img class="" src="images/car-2.png" alt="">
                <p> Напівпричіп тракторний 1ПТС-2 </p>
                <p> Будова: рама зварна конструкція, яка складається з двох боковин , зєднаних між собою двома поперечинами, стояків, балки гідропідйомників, кронштейнів , кутників, тримачів, підсилювачів, запобіжного ланцюга. </p>
                <a class="btn more">Дательніше</a>
            </div>
            <div class="col-4 product-item">
                <h4> Dongfeng DF-244LUX </h4>
                <img class="" src="images/car-2.png" alt="">
                <p> Напівпричіп тракторний 1ПТС-2 </p>
                <p> Будова: рама зварна конструкція, яка складається з двох боковин , зєднаних між собою двома поперечинами, стояків, балки гідропідйомників, кронштейнів , кутників, тримачів, підсилювачів, запобіжного ланцюга. </p>
                <a class="btn more">Дательніше</a>
            </div>
            <div class="col-4 product-item">
                <h4> Dongfeng DF-244LUX </h4>
                <img class="" src="images/car-2.png" alt="">
                <p> Напівпричіп тракторний 1ПТС-2 </p>
                <p> Будова: рама зварна конструкція, яка складається з двох боковин , зєднаних між собою двома поперечинами, стояків, балки гідропідйомників, кронштейнів , кутників, тримачів, підсилювачів, запобіжного ланцюга. </p>
                <a class="btn more">Дательніше</a>
            </div>
            <div class="col-4 product-item">
                <h4> Dongfeng DF-244LUX </h4>
                <img class="" src="images/car-2.png" alt="">
                <p> Напівпричіп тракторний 1ПТС-2 </p>
                <p> Будова: рама зварна конструкція, яка складається з двох боковин , зєднаних між собою двома поперечинами, стояків, балки гідропідйомників, кронштейнів , кутників, тримачів, підсилювачів, запобіжного ланцюга. </p>
                <a class="btn more">Дательніше</a>
            </div>
            <div class="col-4 product-item">
                <h4> Dongfeng DF-244LUX </h4>
                <img class="" src="images/car-2.png" alt="">
                <p> Напівпричіп тракторний 1ПТС-2 </p>
                <p> Будова: рама зварна конструкція, яка складається з двох боковин , зєднаних між собою двома поперечинами, стояків, балки гідропідйомників, кронштейнів , кутників, тримачів, підсилювачів, запобіжного ланцюга. </p>
                <a class="btn more">Дательніше</a>
            </div>
            <div class="col-4 product-item">
                <h4> Dongfeng DF-244LUX </h4>
                <img class="" src="images/car-2.png" alt="">
                <p> Напівпричіп тракторний 1ПТС-2 </p>
                <p> Будова: рама зварна конструкція, яка складається з двох боковин , зєднаних між собою двома поперечинами, стояків, балки гідропідйомників, кронштейнів , кутників, тримачів, підсилювачів, запобіжного ланцюга. </p>
                <a class="btn more">Дательніше</a>          </div>
            <div class="col-4 product-item">
                <h4> Dongfeng DF-244LUX </h4>
                <img class="" src="images/car-2.png" alt="">
                <p> Напівпричіп тракторний 1ПТС-2 </p>
                <p> Будова: рама зварна конструкція, яка складається з двох боковин , зєднаних між собою двома поперечинами, стояків, балки гідропідйомників, кронштейнів , кутників, тримачів, підсилювачів, запобіжного ланцюга. </p>
                <a class="btn more">Дательніше</a>     </div>
            <div class="col-4 product-item">
                <h4> Dongfeng DF-244LUX </h4>
                <img class="" src="images/car-2.png" alt="">
                <p> Напівпричіп тракторний 1ПТС-2 </p>
                <p> Будова: рама зварна конструкція, яка складається з двох боковин , зєднаних між собою двома поперечинами, стояків, балки гідропідйомників, кронштейнів , кутників, тримачів, підсилювачів, запобіжного ланцюга. </p>
                <a class="btn more">Дательніше</a>     </div>
            <div class="col-4 product-item">
                <h4> Dongfeng DF-244LUX </h4>
                <img class="" src="images/car-2.png" alt="">
                <p> Напівпричіп тракторний 1ПТС-2 </p>
                <p> Будова: рама зварна конструкція, яка складається з двох боковин , зєднаних між собою двома поперечинами, стояків, балки гідропідйомників, кронштейнів , кутників, тримачів, підсилювачів, запобіжного ланцюга. </p>
                <a class="btn more">Дательніше</a>      </div>
            <div class="col-4 product-item">
                <h4> Dongfeng DF-244LUX </h4>
                <img class="" src="images/car-2.png" alt="">
                <p> Напівпричіп тракторний 1ПТС-2 </p>
                <p> Будова: рама зварна конструкція, яка складається з двох боковин , зєднаних між собою двома поперечинами, стояків, балки гідропідйомників, кронштейнів , кутників, тримачів, підсилювачів, запобіжного ланцюга. </p>
                <a class="btn more">Дательніше</a>        </div>
            <div class="col-4 product-item">
                <h4> Dongfeng DF-244LUX </h4>
                <img class="" src="images/car-2.png" alt="">
                <p> Напівпричіп тракторний 1ПТС-2 </p>
                <p> Будова: рама зварна конструкція, яка складається з двох боковин , зєднаних між собою двома поперечинами, стояків, балки гідропідйомників, кронштейнів , кутників, тримачів, підсилювачів, запобіжного ланцюга. </p>
                <a class="btn more">Дательніше</a>      </div>
        </div>
    </div>
</div>

<?php include('footer.html') ?>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
<script>
    $('.carousel').carousel({
        interval: 10000000
    })
</script>
</body>
</html>